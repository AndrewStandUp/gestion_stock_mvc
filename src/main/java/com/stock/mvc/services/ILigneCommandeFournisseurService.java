package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.LigneCommandeFounisseur;
public interface ILigneCommandeFournisseurService {

	public LigneCommandeFounisseur save(LigneCommandeFounisseur entity);
	
	public LigneCommandeFounisseur update(LigneCommandeFounisseur entity);
	
	public List<LigneCommandeFounisseur> selectAll();
	
	public List<LigneCommandeFounisseur> selectAll(String sortField, String sort);
	
	public LigneCommandeFounisseur getById(Long id);
	
	public void remove(Long id);
	
	public LigneCommandeFounisseur findOne(String param, Object paramValue);
	
	public LigneCommandeFounisseur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
}
