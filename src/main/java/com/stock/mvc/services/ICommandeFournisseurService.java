package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.CommandeFounisseur;
public interface ICommandeFournisseurService {

	public CommandeFounisseur save(CommandeFounisseur entity);
	
	public CommandeFounisseur update(CommandeFounisseur entity);
	
	public List<CommandeFounisseur> selectAll();
	
	public List<CommandeFounisseur> selectAll(String sortField, String sort);
	
	public CommandeFounisseur getById(Long id);
	
	public void remove(Long id);
	
	public CommandeFounisseur findOne(String param, Object paramValue);
	
	public CommandeFounisseur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
}
