package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entites.Founisseur;
public interface IFournisseurService {

	public Founisseur save(Founisseur entity);
	
	public Founisseur update(Founisseur entity);
	
	public List<Founisseur> selectAll();
	
	public List<Founisseur> selectAll(String sortField, String sort);
	
	public Founisseur getById(Long id);
	
	public void remove(Long id);
	
	public Founisseur findOne(String param, Object paramValue);
	
	public Founisseur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
}
