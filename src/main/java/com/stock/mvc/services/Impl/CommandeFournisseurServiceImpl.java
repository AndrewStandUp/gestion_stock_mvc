package com.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;


import com.stock.mvc.dao.ICommandeFournisseurDao;
import com.stock.mvc.entites.CommandeFounisseur;
import com.stock.mvc.services.ICommandeFournisseurService;

@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService{

	private ICommandeFournisseurDao dao;
	
	public void setDao(ICommandeFournisseurDao dao) {
		this.dao = dao;
	}
	
	@Override
	public CommandeFounisseur save(CommandeFounisseur entity) {
		return dao.save(entity);
	}

	@Override
	public CommandeFounisseur update(CommandeFounisseur entity) {
		return dao.update(entity);
	}

	@Override
	public List<CommandeFounisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<CommandeFounisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeFounisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public CommandeFounisseur findOne(String param, Object paramValue) {
		return dao.findOne(param, paramValue);
	}

	@Override
	public CommandeFounisseur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

			
}
