package com.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCommandeFournisseurDao;
import com.stock.mvc.entites.LigneCommandeFounisseur;
import com.stock.mvc.services.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService{

	private ILigneCommandeFournisseurDao dao;
	
	public void setDao(ILigneCommandeFournisseurDao dao) {
		this.dao = dao;
	}
	
	@Override
	public LigneCommandeFounisseur save(LigneCommandeFounisseur entity) {
		return dao.save(entity);
	}

	@Override
	public LigneCommandeFounisseur update(LigneCommandeFounisseur entity) {
		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeFounisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeFounisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeFounisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public LigneCommandeFounisseur findOne(String param, Object paramValue) {
		return dao.findOne(param, paramValue);
	}

	@Override
	public LigneCommandeFounisseur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

			
}
