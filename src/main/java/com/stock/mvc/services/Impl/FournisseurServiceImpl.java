package com.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IFournisseurDao;
import com.stock.mvc.entites.Founisseur;
import com.stock.mvc.services.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService{

	private IFournisseurDao dao;
	
	public void setDao(IFournisseurDao dao) {
		this.dao = dao;
	}
	
	@Override
	public Founisseur save(Founisseur entity) {
		return dao.save(entity);
	}

	@Override
	public Founisseur update(Founisseur entity) {
		return dao.update(entity);
	}

	@Override
	public List<Founisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Founisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Founisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Founisseur findOne(String param, Object paramValue) {
		return dao.findOne(param, paramValue);
	}

	@Override
	public Founisseur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

			
}
