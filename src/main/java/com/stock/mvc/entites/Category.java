package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class Category implements Serializable{

	@Id
	@GeneratedValue
	private Long IdCategory;
	
	private String code;
	
	private String designation;
	
	@OneToMany(mappedBy="category")
	private List<Article> articles;

	public Category() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return IdCategory;
	}

	public void setId(Long id) {
		IdCategory = id;
	}

	public Long getIdCategory() {
		return IdCategory;
	}

	public void setIdCategory(Long idCategory) {
		IdCategory = idCategory;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
	
}
