package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ligne_vente")
public class LigneVente implements Serializable{

	@Id
	@GeneratedValue
	private Long IdLigneVente;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;

	@ManyToOne
	@JoinColumn(name="vente")
	private Vente vente;
	
	public Long getIdLigneVente() {
		return IdLigneVente;
	}

	public void setIdLigneVente(Long idLigneVente) {
		IdLigneVente = idLigneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
	
}
