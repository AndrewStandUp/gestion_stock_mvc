package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="fournisseur")
public class Founisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long IdFournisseur;
	
	private String nom;
	
	private String prenom;
	
	private String adresse;
	
	private String photo;
	
	private String mail;
	
	@OneToMany(mappedBy="fournisseur")
	private List<CommandeFounisseur> commandeFounisseurs;
	
	public Founisseur() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getIdFournisseur() {
		return IdFournisseur;
	}

	public void setIdFournisseur(Long idFournisseur) {
		IdFournisseur = idFournisseur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public List<CommandeFounisseur> getCommandeFounisseurs() {
		return commandeFounisseurs;
	}

	public void setCommandeFounisseurs(List<CommandeFounisseur> commandeFounisseurs) {
		this.commandeFounisseurs = commandeFounisseurs;
	}

	
	
}
