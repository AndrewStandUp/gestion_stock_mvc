<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder='<fmt:message
						key="common.recherche" />'>
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div> <!-- /input-group -->
			</li>
			<c:url value="/home/" var="home" />
			<li><a href="${home}"><i class="fa fa-dashboard fa-fw"></i>
					<fmt:message key="common.dashbord" /></a></li>
					<c:url value="/article/" var="article" />
			<li><a href="${article}"><i class="fa fa-dashboard fa-fw"></i>
					<fmt:message key="common.article" /></a></li>
			<li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> <fmt:message
						key="common.client" /><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
				<c:url value="/client/" var="cmdClient" />
					<li><a href="${cmdClient}"><fmt:message
								key="common.client.commande" /></a></li>
				</ul>
			
			
			<li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> <fmt:message
						key="common.fournisseur" /><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
				<c:url value="/fournisseur/" var="cmdFouns" />
					<li><a href="${cmdFouns}"><fmt:message
								key="common.fournisseur.commande" /></a></li>
				</ul> <!-- /.nav-second-level --></li>
			
			<li><a href="tables.html"><i class="fa fa-table fa-fw"></i>
					<fmt:message key="common.stocks" /></a></li>
			<li><a href="forms.html"><i class="fa fa-edit fa-fw"></i> <fmt:message
						key="common.vente" /></a></li>
			
			
			<li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> <fmt:message
						key="common.parrametrage" /><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
				<c:url value="/paramUser/" var="prmUser" />
					<li><a href="${prmUser}"><fmt:message
								key="common.parrametrage.utilisateur" /></a></li>
								<c:url value="/paramCate/" var="prmCate" />
					<li><a href="${prmCate}"><fmt:message
								key="common.parrametrage.category" /></a></li>
				</ul> <!-- /.nav-second-level --></li>
			
			
				
			</ul>
			<!-- /.nav-second-level -->
			</li>

		</ul>
		<!-- /.nav-second-level -->
		</li>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>